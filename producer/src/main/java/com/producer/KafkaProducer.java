package com.producer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class KafkaProducer {

    private Producer<String,String> inner;
    //public KafkaProducer() throws Exception{
    public KafkaProducer(){
        Properties properties = new Properties();
        //properties.load(ClassLoader.getSystemResourceAsStream("producer.properties"));
        //InputStream inputStream = getClass().getClassLoader().getResourceAsStream("config/config.properties");
        InputStream inputStream = getClass().getClassLoader().getSystemResourceAsStream("producer.properties");

        if (inputStream != null) {
            try {
                properties.load(inputStream);
                System.out.println("zk: " + properties.get("zookeeper.connect"));
            } catch (java.io.IOException e) {
                e.printStackTrace();
            }
        } else {
            //throw new FileNotFoundException("property file 'config/config.properties' not found!");
            System.out.println("property file 'resources/producer.properties' not found!");
        }
        ProducerConfig config = new ProducerConfig(properties);
        inner = new Producer<String, String>(config);
    }

    
    public void send(String topicName,String message) {
        if(topicName == null || message == null){
            return;
        }
        KeyedMessage<String, String> km = new KeyedMessage<String, String>(topicName,message);
        inner.send(km);
    }
    
    public void send(String topicName,Collection<String> messages) {
        if(topicName == null || messages == null){
            return;
        }
        if(messages.isEmpty()){
            return;
        }
        List<KeyedMessage<String, String>> kms = new ArrayList<KeyedMessage<String, String>>();
        for(String entry : messages){
            KeyedMessage<String, String> km = new KeyedMessage<String, String>(topicName,entry);
            kms.add(km);
        }
        inner.send(kms);
    }
    
    public void close(){
        inner.close();
    }
    
    /**
     * @param args
     */
    public void run() {
        int i=0;
        while(true){
            send("test-topic", "this is a sample" + i);
            i++;
            //Thread.sleep(2000);
        }
    }
}
