package com.producer;

//import junit.framework.Test;
//import junit.framework.TestCase;
//import junit.framework.TestSuite;
//
///**
// * Unit test for simple App.
// */
//public class AppTest 
//    extends TestCase
//{
//    /**
//     * Create the test case
//     *
//     * @param testName name of the test case
//     */
//    public AppTest( String testName )
//    {
//        super( testName );
//    }
//
//    /**
//     * @return the suite of tests being tested
//     */
//    public static Test suite()
//    {
//        return new TestSuite( AppTest.class );
//    }
//
//    /**
//     * Rigourous Test :-)
//     */
//    public void testApp()
//    {
//        assertTrue( true );
//    }
//}

import com.producer.KafkaProducer;

public class KafkaProducerTest {
    public static void main(String[] args) {
        KafkaProducer p = null;
        try{
            p = new KafkaProducer();
            p.run();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            if(p != null){
                p.close();
            }
        }
    } 
}
