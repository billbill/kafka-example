Kafka's simple producer and consumer example

STEP: 
1. mvn clean package 
2. copy the kafka-producer-1.0-jar-with-dependencies.jar and kafka-consumer-1.0-jar-with-dependencies.jar to kafka cluster 
3. run consumer: java -jar kafka-consumer-1.0-jar-with-dependencies.jar 
4. run producer: java -jar kafka-producer-1.0-jar-with-dependencies.jar
